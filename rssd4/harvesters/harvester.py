import logging
import os
import sqlite3

from datetime import datetime


class Harvester:
    """Parent of all harvesters.

    Should be used as a sample, if you want to create new harvester.
    """
    ISO8601FORMAT = '%Y-%m-%dT%H:%M:%S%z'

    def __init__(self, feed_id, url, db_dir, db_name, max_items=100,
                 table_name='data'):
        """Initializes the current harvester to retrieve data from given
        URL and write it to database with given record_id.

        Input parameters:
         feed_id - ID of category/feed name pair.
         max_items - maximal items to read in one harvester iteration.
        """
        logging.debug('Harvester [%s, %s, %i] initiating...' % (
            self.__class__.__name__, url, feed_id))
        self.db_name = db_name
        self.table_name = table_name
        self.db_dir = db_dir
        self.feed_id = feed_id
        self.url = url
        self.max_items = max_items
        self.check_db_existence()
        self.connection = sqlite3.connect(database=db_dir + db_name,
                                          check_same_thread=False)
        cursor = self.connection.cursor()
        create_table_statement = 'CREATE TABLE IF NOT EXISTS %s (feed_id INTEGER, ' \
                                 'datetime TEXT, author TEXT, header TEXT, ' \
                                 'body TEXT, new INTEGER);' % table_name
        create_view_statement = 'CREATE VIEW IF NOT EXISTS %s AS SELECT ' \
                                'rowid, feed_id, datetime, author, header, body, new ' \
                                'FROM %s WHERE new=1;' % (
                                    'unread_' + table_name, table_name)
        cursor.execute('PRAGMA journal_mode=WAL;')
        cursor.execute(create_table_statement)
        cursor.execute(create_view_statement)
        self.connection.commit()
        cursor.close()
        logging.debug("[%s, %i] Initialized %s database" % (
            self.__class__.__name__,
            self.feed_id,
            db_dir + db_name))

    def check_db_existence(self):
        if os.path.isdir(self.db_dir) is False:
            if os.path.exists(self.db_dir) is True:
                logging.error("[%s, %i] %s already exists but it is not a "
                              "directory" % (self.__class__.__name__,
                                             self.feed_id, self.db_dir))
                raise FileExistsError('%s already exists but it is a file' %
                                      self.db_dir)
            else:
                os.makedirs(self.db_dir)
                logging.debug("[%s, %i] Created %s directory." % (
                    self.__class__.__name__, self.feed_id, self.db_dir))
        if os.path.exists(self.db_dir + self.db_name) is False:
            logging.debug('[%s, %i] %s not exists!' % (
                self.__class__.__name__, self.feed_id, self.db_dir + self.db_name))
            open(self.db_dir + self.db_name, 'w')
            logging.info('[%s, %i] Created database %s' % (
                self.__class__.__name__, self.feed_id, self.db_dir + self.db_name))

    def exec_once(self):
        latest_time = self.get_latest_save_time()
        self.write_to_database(
            filter(lambda x: x[0] > latest_time,
                   self.get_new_data()))

    def write_to_database(self, data_list):
        insert_statement = 'INSERT INTO %s (feed_id, datetime, author, ' \
                           'header, body, new) VALUES (?, ?, ?, ?, ?, ?);' % \
                           self.table_name
        cursor = self.connection.cursor()
        for (date_time, author, header, body) in data_list:
            cursor.execute(insert_statement,
                           (
                               str(self.feed_id),
                               date_time.strftime(self.ISO8601FORMAT),
                               author,
                               header,
                               body,
                               1
                           ))
            logging.debug('[%s, %i] Adding new data: %s %s %s.' % (
                self.__class__.__name__, self.feed_id,
                date_time.strftime(self.ISO8601FORMAT), author,  header))
        self.connection.commit()
        cursor.close()

    def get_latest_save_time(self):
        select_statement = 'SELECT MAX(datetime) FROM %s ' \
                           'WHERE feed_id = %s;' % (self.table_name,
                                                    self.feed_id)
        cursor = self.connection.cursor()
        datetime_rows = cursor.execute(select_statement)
        latest_time = datetime_rows.fetchone()
        # logging.debug('[%s, %i] latest save time from DB: %s',
        #             self.__class__.__name__, self.feed_id, result[0])
        if latest_time[0] is None:
            result = datetime.strptime('1970-01-01T00:00:00+0000', self.ISO8601FORMAT)
        else:
            result = datetime.strptime(latest_time[0], self.ISO8601FORMAT)
        cursor.close()
        return result

    def get_new_data(self):
        yield (
            datetime.strptime('2014-12-13T12:13:17+0800', self.ISO8601FORMAT),
            'authorX',
            'headerX',
            'bodyX')
