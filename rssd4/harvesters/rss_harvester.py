import feedparser

from datetime import datetime
from dateutil import tz


from rssd4.harvesters.harvester import Harvester


class RSSHarvester(Harvester):
    rss_feed = None

    def exec_once(self):
        self.rss_feed = feedparser.parse(self.url)
        super(RSSHarvester, self).exec_once()

    def get_new_data(self):
        for rss_item in self.rss_feed.entries:
            try:
                date_time = datetime.strptime(rss_item.published, '%a, %d %b %Y %H:%M:%S %z')
            except ValueError:
                # We got datetime with timezone abbreviation
                # (like Mon, 12 Oct 2012 11:02:33 CST),
                # not with offset from UTC
                # (like Mon, 12 Oct 2012 11:02:33 +0800), but we need
                # date with offset for our methods.
                date_time = ' '.join(rss_item.published.split()[0:-1])
                tz_abbrev = rss_item.published.split()[-1]
                tz_abbrev = tz.gettz(tz_abbrev)
                date_time = datetime.strptime(date_time, '%a, %d %b %Y %H:%M:%S')
                date_time = date_time.replace(tzinfo=tz_abbrev)
            try:
                author = rss_item.author
            except (AttributeError, KeyError):
                author = ''
            yield date_time, author, rss_item.title, rss_item.summary
