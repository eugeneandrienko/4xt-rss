import logging
import os
import sqlite3
import time

from rssd4.harvesters.harvester import Harvester
from rssd4.harvesters.rss_harvester import RSSHarvester


def read_harvesters_settings(db_dir, db_name):
    harvesters_settings = []
    if os.path.exists(db_dir + db_name) is False:
        logging.warning('Database (%s) with settings does not exists' % db_name)
        try:
            os.makedirs(db_dir)
        except FileExistsError:
            logging.info('Directory %s already exists' % db_dir)
        open(db_dir + db_name, 'w')
        connection = sqlite3.connect(database=db_dir + db_name,
                                     check_same_thread=False)
        cursor = connection.cursor()
        cursor.execute('PRAGMA journal_mode=WAL;')
        cursor.execute('CREATE TABLE IF NOT EXISTS {table_name} ({columns});'.format(
                        table_name='harvesters_settings',
                        columns='feed_id INTEGER PRIMARY KEY ASC '
                                'AUTOINCREMENT, category TEXT, '
                                'feed_name TEXT, url TEXT, harvester_type TEXT'))
        connection.commit()
        cursor.close()
        logging.info('Created database %s' % db_dir + db_name)
    else:
        connection = sqlite3.connect(database=db_dir + db_name,
                                     check_same_thread=False)
        cursor = connection.cursor()
        for row in cursor.execute('SELECT feed_id, url, harvester_type '
                                  'FROM harvesters_settings;'):
            logging.debug('Read data from %s: %s' % (db_name, row))
            harvesters_settings.append(row)
        cursor.close()
        logging.info('Read settings from harvester database')
    return harvesters_settings


def initialize_harvesters(harvesters_settings, db_dir, db_name):
    harvesters = []
    harvester_type_table = {
        'simple': Harvester,
        'rss': RSSHarvester
    }
    for harvester_setting in harvesters_settings:
        try:
            harvester = harvester_type_table[harvester_setting[2]](
                feed_id=harvester_setting[0],
                url=harvester_setting[1],
                db_dir=db_dir,
                db_name=db_name)
        except KeyError:
            logging.error('Unknown harvester type: %s' % harvester_setting[2])
            continue
        harvesters.append(harvester)
    return harvesters


modification_time = None


def try_update_harvesters_settings(harvesters, db_dir, db_name, data_db_name):
    global modification_time
    stat = os.stat(db_dir + db_name)
    if modification_time is not None:
        if stat.st_mtime > modification_time:
            logging.info('Found harvester settings update, reloading harvesters')
            modification_time = stat.st_mtime
            h_settings = read_harvesters_settings(db_dir, db_name)
            harvesters = initialize_harvesters(h_settings, db_dir, data_db_name)
    else:
        modification_time = stat.st_mtime
    return harvesters


def execute_harvesters(harvesters, harvester_delay=5):
    for harvester in harvesters:
        harvester.exec_once()
        time.sleep(harvester_delay)


def main(lock, settings):
    harvester_db = {
        'db_dir': settings['database_directory'],
        'db_name': settings['h_settings_db']
    }
    data_db = {
        'db_dir': settings['database_directory'],
        'db_name': settings['data_db']
    }
    h_settings = read_harvesters_settings(**harvester_db)
    harvesters = initialize_harvesters(h_settings, **data_db)
    lock.release()
    logging.debug("Lock released in worker process")
    while True:
        harvesters = try_update_harvesters_settings(
            harvesters,
            data_db_name=settings['data_db'],
            **harvester_db
        )
        execute_harvesters(harvesters, settings['harvester_delay'])
        time.sleep(settings['update_delay'])
