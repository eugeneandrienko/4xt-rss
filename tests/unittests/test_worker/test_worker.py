"""Unit-tests for worker/worker.py module."""

import unittest
import rssd4.worker

from unittest.mock import call
from unittest.mock import patch
from unittest.mock import MagicMock

from rssd4.worker.worker import execute_harvesters
from rssd4.worker.worker import initialize_harvesters
from rssd4.worker.worker import read_harvesters_settings
from rssd4.worker.worker import try_update_harvesters_settings


class ReadHarvestersSettingsTestCase(unittest.TestCase):
    """Tests for read_harvesters_settings() function."""

    @patch('rssd4.worker.worker.sqlite3.connect')
    @patch('rssd4.worker.worker.os.path.exists')
    def test_normal_operation_db_exists(self, mock_exists, mock_connect):
        """Function should read settings from database and return array with settings."""
        expected_value = [
            'test_data_1',
            'test_data_2',
            'test_data_3'
        ]
        mock_exists.return_value = True
        mock_connection = MagicMock()
        mock_connect.return_value = mock_connection
        mock_cursor = MagicMock()
        mock_connection.cursor.return_value = mock_cursor
        mock_cursor.execute.return_value = expected_value
        with patch('rssd4.worker.worker.logging'):
            output_value = read_harvesters_settings('test_db_dir', 'test_db_name')
        self.assertListEqual(output_value, expected_value)

    @patch('rssd4.worker.worker.os.makedirs')
    @patch('rssd4.worker.worker.sqlite3.connect')
    @patch('rssd4.worker.worker.os.path.exists')
    def test_normal_operation_db_not_exists(self, mock_exists, mock_connect, mock_makedirs):
        """Function should create new database and return empty array."""
        expected_value = []
        mock_exists.return_value = False
        mock_connection = MagicMock()
        mock_connect.return_value = mock_connection
        mock_cursor = MagicMock()
        mock_connection.cursor.return_value = mock_cursor
        with patch('rssd4.worker.worker.logging'), \
                patch('rssd4.worker.worker.open', create=True):
            output_value = read_harvesters_settings('test_db_dir', 'test_db_name')
        self.assertListEqual(output_value, expected_value)

    @patch('rssd4.worker.worker.os.makedirs')
    @patch('rssd4.worker.worker.sqlite3.connect')
    @patch('rssd4.worker.worker.os.path.exists')
    def test_db_directory_exists(self, mock_exists, mock_connect, mock_makedirs):
        """If database directory exists but database is not -
        function should just write to log, create new database
        and return empty array.
        """
        expected_value = []
        mock_exists.return_value = False
        mock_makedirs.side_effect = FileExistsError
        mock_connection = MagicMock()
        mock_connect.return_value = mock_connection
        mock_cursor = MagicMock()
        mock_connection.cursor.return_value = mock_cursor
        with patch('rssd4.worker.worker.logging') as mock_logging, \
                patch('rssd4.worker.worker.open', create=True):
            output_value = read_harvesters_settings('test_db_dir', 'test_db_name')
            calls = [
                call('Directory test_db_dir already exists'),
                call('Created database test_db_dirtest_db_name')
            ]
            mock_logging.info.assert_has_calls(calls)
        self.assertListEqual(output_value, expected_value)

    @patch('rssd4.worker.worker.os.makedirs')
    @patch('rssd4.worker.worker.os.path.exists')
    def test_makedirs_error(self, mock_exists, mock_makedirs):
        """If os.makedirs() failed - function should raise exception."""
        mock_exists.return_value = False
        mock_makedirs.side_effect = OSError
        with patch('rssd4.worker.worker.logging'):
            self.assertRaises(OSError, read_harvesters_settings, '', '')


class InitializeHarvestersTestCase(unittest.TestCase):
    """Tests for initialize_harvesters() function."""

    @patch('rssd4.worker.worker.Harvester')
    def test_normal_operation(self, mock_harvester):
        """Function should just return an array with list of initialized objects."""
        mock_harvester.return_value = 'successful_test'
        harvesters_settings = (
            (1, 'feed_url_1', 'simple'),
            (2, 'feed_url_2', 'simple'),
            (3, 'feed_url_3', 'simple')
        )
        expected_result = [
            'successful_test',
            'successful_test',
            'successful_test'
        ]
        result = initialize_harvesters(harvesters_settings, 'db_dir', 'db_name')
        self.assertListEqual(expected_result, result, msg="Error in harvesters' initialization.")

    @patch('rssd4.worker.worker.logging')
    @patch('rssd4.worker.worker.Harvester')
    def test_unknown_harvester_type(self, mock_harvester, mock_logging):
        """Function should just return an array with list of initialized objects,
        with logging all unknown harvesters' types.
        """
        mock_harvester.return_value = 'successful_test'
        harvesters_settings = (
            (1, 'feed_url_1', 'simple'),
            (2, 'feed_url_2', 'simple'),
            (3, 'feed_url_3', 'unknown')
        )
        expected_result = [
            'successful_test',
            'successful_test'
        ]
        result = initialize_harvesters(harvesters_settings, 'db_dir', 'db_name')
        self.assertListEqual(expected_result, result, msg="Error in harvesters' initialization.")
        mock_logging.error.assert_called_once_with('Unknown harvester type: unknown')


class TryUpdateHarvestersSettingsTestCase(unittest.TestCase):
    """Tests for try_update_harvesters_settings() function."""

    @patch('rssd4.worker.worker.os.stat')
    def test_db_not_modified(self, mock_stat):
        """Function should just return given list of harvesters."""
        rssd4.worker.worker.modification_time = 5
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 5
        mock_stat.return_value = mock_stat_result
        harvesters = [1, 2, 3, 4, 5]
        result = try_update_harvesters_settings(harvesters, '', '', None)
        self.assertListEqual(result, harvesters, msg="Harvesters lists not equal")

    @patch('rssd4.worker.worker.os.stat')
    def test_db_modified(self, mock_stat):
        """Function should reinitialize harvesters and return
        new list of harvesters.
        """
        rssd4.worker.worker.modification_time = 5
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 6
        mock_stat.return_value = mock_stat_result
        harvesters = [1, 2, 3, 4, 5]
        expected_result = [6, 7, 8, 9, 0]
        with patch('rssd4.worker.worker.logging') as mock_logging, \
                patch('rssd4.worker.worker.read_harvesters_settings') as mock_read_settings, \
                patch('rssd4.worker.worker.initialize_harvesters') as mock_initialize:
            mock_read_settings.return_value = None
            mock_initialize.return_value = expected_result
            result = try_update_harvesters_settings(harvesters, '', '', None)
            mock_logging.info.assert_called_once_with('Found harvester settings update, reloading harvesters')
        self.assertListEqual(result, expected_result, msg="Harvesters lists not equal")

    @patch('rssd4.worker.worker.os.stat')
    def test_first_run(self, mock_stat):
        """Function should set modification time and return
        given list of harvesters.
        """
        rssd4.worker.worker.modification_time = None
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 11
        mock_stat.return_value = mock_stat_result
        harvesters = [1, 2, 3, 4, 5]
        result = try_update_harvesters_settings(harvesters, '', '', None)
        self.assertListEqual(harvesters, result, msg="Harvesters lists not equal")
        self.assertEqual(rssd4.worker.worker.modification_time, 11)


class ExecuteHarvestersTestCase(unittest.TestCase):
    """Test for execute_harvesters() function."""

    def test_normal_operation(self):
        """Function should just returns, without any side effects."""
        with patch('rssd4.worker.worker.time.sleep') as mock_sleep:
            mock_harvester = MagicMock()
            harvesters = [mock_harvester,
                          mock_harvester,
                          mock_harvester]
            execute_harvesters(harvesters)


if __name__ == '__main__':
    unittest.main()
