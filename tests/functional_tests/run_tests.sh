PATH_TO_REPO="/home/drag0n/git_repos/4xt-rss"
PYTHONPATH=$PYTHONPATH:$PATH_TO_REPO
export PYTHONPATH

for ftest in test_*.py; do
    echo "Starting $ftest..."
    python3 ./$ftest
    echo
done

