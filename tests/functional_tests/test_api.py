"""Functional tests for REST API."""

import bottle
import json
import logging
import multiprocessing
import os
import requests
import shutil
import sqlite3
import time

from rssd4.api.functions import HarvestersSettings, HarvestersData
from rssd4.api.routes import Categories, Feeds, Route


IP = '127.0.0.1'
PORT = 8080
DB_DIR = './test_db_dir/'
API_PREFIX = Route.route_prefix
LOGIN = 'test_login'
PASSWORD = 'test_password'


def test_edit_category_name():
    """Test for /categories/<category_name> POST API call."""
    requests.post('http://%s:%d%s/categories/test_category' % (IP, PORT, API_PREFIX),
                  data=json.dumps({
                      'new_name': 'new_name'
                  }),
                  headers={
                      'Content-type': 'application/json',
                      'Accept': 'text/plain'
                  },
                  auth=(LOGIN, PASSWORD))
    connection = sqlite3.connect(database=DB_DIR + 'h_settings.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    select_result = cursor.execute('SELECT * FROM harvesters_settings;')
    result_row = select_result.fetchone()
    cursor.close()
    if result_row[1] == 'new_name':
        return True
    else:
        return False


def test_delete_category():
    """Test for /categories/<category_name> DELETE API call."""
    requests.delete('http://%s:%d%s/categories/test_category' % (IP, PORT, API_PREFIX),
                    auth=(LOGIN, PASSWORD))
    connection = sqlite3.connect(database=DB_DIR + 'h_settings.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    select_result = cursor.execute('SELECT * FROM harvesters_settings;')
    result_row = select_result.fetchall()
    cursor.close()
    if len(result_row) == 0:
        return True
    else:
        return False


def test_categories_feeds_list():
    """Test for /feeds GET API call."""
    response = requests.get('http://%s:%d%s/feeds' % (IP, PORT, API_PREFIX),
                            auth=(LOGIN, PASSWORD))
    expected_json = {'feeds_list': []}
    for i in range(0, 10):
        expected_json['feeds_list'].append(
            {
                'feed_id': i + 1,
                'category': 'test_category',
                'feed_name': 'name%d' % i
            }
        )
    if expected_json == response.json():
        return True
    else:
        return False


def test_add_new_feed():
    """Test for /feeds POST API call."""
    requests.post('http://%s:%d%s/feeds' % (IP, PORT, API_PREFIX),
                  data=json.dumps({
                      'category': 'test_category',
                      'feed_name': 'test_name',
                      'url': 'http://example.org',
                      'harvester_type': 'simple'
                  }),
                  headers={
                      'Content-type': 'application/json',
                      'Accept': 'text/plain'
                  },
                  auth=(LOGIN, PASSWORD))
    connection = sqlite3.connect(database=DB_DIR + 'h_settings.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    select_result = cursor.execute('SELECT * FROM harvesters_settings '
                                   'WHERE feed_name=\'%s\';' % 'test_name')
    result_row = select_result.fetchone()
    cursor.close()
    if result_row[1] == 'test_category' and \
            result_row[2] == 'test_name' and \
            result_row[3] == 'http://example.org' and \
            result_row[4] == 'simple':
        return True
    else:
        return False


def test_edit_feed_name():
    """Test for /feeds/<feed_id> POST API call."""
    requests.post('http://%s:%d%s/feeds/1' % (IP, PORT, API_PREFIX),
                  data=json.dumps({
                      'new_name': 'new_name'
                  }),
                  headers={
                      'Content-type': 'application/json',
                      'Accept': 'text/plain'
                  },
                  auth=(LOGIN, PASSWORD))
    connection = sqlite3.connect(database=DB_DIR + 'h_settings.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    select_result = cursor.execute('SELECT * FROM harvesters_settings '
                                   'WHERE feed_id=1;')
    result_row = select_result.fetchone()
    cursor.close()
    if result_row[0] == 1 and \
            result_row[2] == 'new_name':
        return True
    else:
        return False


def test_delete_feed():
    """Test for /feeds/<feed_id> DELETE API call."""
    requests.delete('http://%s:%d%s/feeds/1' % (IP, PORT, API_PREFIX),
                    auth=(LOGIN, PASSWORD))
    connection = sqlite3.connect(database=DB_DIR + 'h_settings.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    select_result = cursor.execute('SELECT * FROM harvesters_settings '
                                   'WHERE feed_id=1;')
    result_rows = select_result.fetchall()
    cursor.close()
    if len(result_rows) == 0:
        return True
    else:
        return False


def test_get_all_items():
    """Test for /feeds/<feed_id>/items GET API call."""
    response = requests.get('http://%s:%d%s/feeds/1/items' % (IP, PORT, API_PREFIX),
                            auth=(LOGIN, PASSWORD))
    if len(response.json()['items_list']) == 10:
        return True
    else:
        return False


def test_get_unread_items():
    """Test for /feeds/<feed_id>/unread_items GET API call."""
    hd = HarvestersData(DB_DIR, 'data.db')
    hd.set_item_as_read(1)
    response = requests.get('http://%s:%d%s/feeds/1/unread_items' % (IP, PORT, API_PREFIX),
                            auth=(LOGIN, PASSWORD))
    if len(response.json()['items_list']) == 9:
        return True
    else:
        return False


def test_set_item_as_read():
    """Test for /feeds/<feed_id>/items/<item_id> POST API call."""
    requests.post('http://%s:%d%s/feeds/1/items/1' % (IP, PORT, API_PREFIX),
                  auth=(LOGIN, PASSWORD))
    hd = HarvestersData(DB_DIR, 'data.db')
    if len(list(hd.get_unread_items(1))) == 9:
        return True
    else:
        return False


def start_api(lock):
    """Start API part of application."""
    lock.acquire()
    categories = Categories(LOGIN, PASSWORD, DB_DIR, 'h_settings.db')
    feeds = Feeds(LOGIN, PASSWORD, DB_DIR, 'h_settings.db', 'data.db')
    lock.release()
    bottle.run(host=IP, port=PORT, quiet=True)


def initialize_database():
    try:
        os.makedirs(DB_DIR)
    except FileExistsError:
        pass

    # Settings database initialization
    open(DB_DIR + 'h_settings.db', 'w')
    connection = sqlite3.connect(database=DB_DIR + 'h_settings.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    cursor.execute('PRAGMA journal_mode=WAL;')
    cursor.execute('DROP TABLE IF EXISTS {table_name};'.format(
        table_name='harvesters_settings'))
    cursor.execute('CREATE TABLE {table_name} ({columns});'.format(
        table_name='harvesters_settings',
        columns='feed_id INTEGER PRIMARY KEY ASC '
                'AUTOINCREMENT, category TEXT, '
                'feed_name TEXT, url TEXT, harvester_type TEXT'))
    connection.commit()
    cursor.close()
    hs = HarvestersSettings(DB_DIR, 'h_settings.db')
    for i in range(0, 10):
        hs.add_new_feed('test_category', 'name%d' % i,
                        'http://example%d.org' % i, 'rss')

    # Data database initialization
    open(DB_DIR + 'data.db', 'w')
    connection = sqlite3.connect(database=DB_DIR + 'data.db',
                                 check_same_thread=False)
    cursor = connection.cursor()
    cursor.execute('PRAGMA journal_mode=WAL;')
    cursor.execute('DROP TABLE IF EXISTS {table_name};'.format(
        table_name='data'))
    cursor.execute('CREATE TABLE {table_name} ({columns});'.format(
        table_name='data',
        columns='feed_id INTEGER, datetime TEXT, author TEXT, header TEXT, '
                'body TEXT, new INTEGER'))
    cursor.execute(
        'CREATE VIEW IF NOT EXISTS {view_name} AS SELECT {select_statement};'.format(
            view_name='unread_data',
            select_statement='rowid, feed_id, datetime, author, header, body, new FROM data WHERE new=1'
        ))
    for i in range(0, 10):
        cursor.execute(
            'INSERT INTO data (feed_id, datetime, author, header, body, new) VALUES (?, ?, ?, ?, ?, ?);',
            (
                1,
                '2014-12-13T12:13:1%d:+0800' % i,
                'author',
                'header%d' % i,
                'body%d' % i,
                1
            ))
    connection.commit()
    cursor.close()


def deinitialize_database():
    shutil.rmtree(DB_DIR)


def start_api_tests():
    test_list = {
        'POST /categories/<category_name>': test_edit_category_name,
        'DELETE /categories/<category_name>': test_delete_feed,
        'GET /feeds': test_categories_feeds_list,
        'POST /feeds': test_add_new_feed,
        'POST /feeds/<feed_id>': test_edit_feed_name,
        'DELETE /feeds/<feed_id>': test_delete_feed,
        'GET /feeds/<feed_id>/items': test_get_all_items,
        'GET /feeds/<feed_id>/unread_items': test_get_unread_items,
        'POST /feeds/<feed_id>/items/<item_id>': test_set_item_as_read
    }
    db_lock = multiprocessing.Lock()

    db_lock.acquire()
    initialize_database()
    logging.debug('Test database initialized!')
    api_process = multiprocessing.Process(target=start_api, args=(db_lock,))
    api_process.start()
    db_lock.release()
    time.sleep(5)  # Wait, while Bottle starting up...
    logging.info('API tests started.')
    for k, v in test_list.items():
        print("{key}: ".format(key=k), end='')
        print("{value}".format(value={True: 'PASSED', False: 'FAIL'}[v()]))
        initialize_database()
    logging.info('Tests ended')
    api_process.terminate()
    logging.debug('Test API process terminated!')
    deinitialize_database()
    logging.debug('Test database removed.')


if __name__ == '__main__':
    start_api_tests()
